<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Reader;
use App\Models\Users;

class CsvController extends Controller
{
    public function uploadCsv(Request $request, $id)
    {
        $csvFile = $request->file('csv_file');
        $reader = Reader::createFromPath($csvFile->path());

        foreach ($reader as $row) {
            error_log($row[0]);
            if($row[0] != "UID"){
                $user = new Users;
                $user->UID = $row[0];
                $user->challenge_id = $id;
                $user->save();
            }
        }

        return response('User inserted successfully');
        //return response()->json([$reader]);

    }
}
