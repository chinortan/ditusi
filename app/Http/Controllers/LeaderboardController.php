<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LeaderboardController extends Controller
{
    public function getUserInfo($userId)
    {
        $response = Http::get("https://enka.network/api/uid/".$userId);
        return json_decode($response->getBody(), true);
    }

    public function getUserScore($userId, $avatarId)
    {
        $userInfo = $this->getUserInfo($userId);
        $showAvatarInfoList = collect($userInfo['avatarInfoList']);
        $avatarInfo = $showAvatarInfoList->where('avatarId', $avatarId)->first();

        $data = array(
            'critRatePercentNow' => $avatarInfo['fightPropMap'][20] * 100,
            'critDmgPercentNow' => $avatarInfo['fightPropMap'][22] * 100,
            'emNow' => $avatarInfo['fightPropMap'][28] * 100,
            'erNow' => $avatarInfo['fightPropMap'][23] * 100,
            'hpNow' => $avatarInfo['fightPropMap'][2000],
            'defNow' => $avatarInfo['fightPropMap'][2002],
            'atkNow' => $avatarInfo['fightPropMap'][2001],
            'physicalDmgBonus' => $avatarInfo['fightPropMap'][30] * 100,
            'pyroDmgBonus' => $avatarInfo['fightPropMap'][40] * 100,
            'electroDmgBonus' => $avatarInfo['fightPropMap'][41] * 100,
            'hydroDmgBonus' => $avatarInfo['fightPropMap'][42] * 100,
            'dendroDmgBonus' => $avatarInfo['fightPropMap'][43] * 100,
            'anemoDmgBonus' => $avatarInfo['fightPropMap'][44] * 100,
            'geoDmgBonus' => $avatarInfo['fightPropMap'][45] * 100,
            'cryoDmgBonus' => $avatarInfo['fightPropMap'][46] * 100,
            'weaponNow' => $avatarInfo['equipList']
        );



        print_r($data);
    }

    public function getCharacterInfoByName($avatarId)
    {
        $text = "_key;Element;Name;SideIconName;NameTextMapHash
        10000002;Ice;Kamisato Ayaka;UI_AvatarIcon_Side_Ayaka;1006042610
        10000003;Wind;Jean;UI_AvatarIcon_Side_Qin;3221566250
        10000005;Wind;Traveler;UI_AvatarIcon_Side_PlayerBoy;1533656818
        10000006;Electric;Lisa;UI_AvatarIcon_Side_Lisa;3344622722
        10000007;Wind;Traveler;UI_AvatarIcon_Side_PlayerGirl;3816664530
        10000014;Water;Barbara;UI_AvatarIcon_Side_Barbara;3775299170
        10000015;Ice;Kaeya;UI_AvatarIcon_Side_Kaeya;4119663210
        10000016;Fire;Diluc;UI_AvatarIcon_Side_Diluc;3608180322
        10000020;Electric;Razor;UI_AvatarIcon_Side_Razor;4160147242
        10000021;Fire;Amber;UI_AvatarIcon_Side_Ambor;1966438658
        10000022;Wind;Venti;UI_AvatarIcon_Side_Venti;2466140362
        10000023;Fire;Xiangling;UI_AvatarIcon_Side_Xiangling;1130996346
        10000024;Electric;Beidou;UI_AvatarIcon_Side_Beidou;2646367730
        10000025;Water;Xingqiu;UI_AvatarIcon_Side_Xingqiu;4197635682
        10000026;Wind;Xiao;UI_AvatarIcon_Side_Xiao;1021947690
        10000027;Rock;Ningguang;UI_AvatarIcon_Side_Ningguang;4127888970
        10000029;Fire;Klee;UI_AvatarIcon_Side_Klee;3339083250
        10000030;Rock;Zhongli;UI_AvatarIcon_Side_Zhongli;3862787418
        10000031;Electric;Fischl;UI_AvatarIcon_Side_Fischl;3277782506
        10000032;Fire;Bennett;UI_AvatarIcon_Side_Bennett;968893378
        10000033;Water;Tartaglia;UI_AvatarIcon_Side_Tartaglia;3847143266
        10000034;Rock;Noelle;UI_AvatarIcon_Side_Noel;1921418842
        10000035;Ice;Qiqi;UI_AvatarIcon_Side_Qiqi;168956722
        10000036;Ice;Chongyun;UI_AvatarIcon_Side_Chongyun;2876340530
        10000037;Ice;Ganyu;UI_AvatarIcon_Side_Ganyu;2679781122
        10000038;Rock;Albedo;UI_AvatarIcon_Side_Albedo;4108620722
        10000039;Ice;Diona;UI_AvatarIcon_Side_Diona;1468367538
        10000041;Water;Mona;UI_AvatarIcon_Side_Mona;1113306282
        10000042;Electric;Keqing;UI_AvatarIcon_Side_Keqing;1864015138
        10000043;Wind;Sucrose;UI_AvatarIcon_Side_Sucrose;1053433018
        10000044;Fire;Xinyan;UI_AvatarIcon_Side_Xinyan;4273845410
        10000045;Ice;Rosaria;UI_AvatarIcon_Side_Rosaria;4260733330
        10000046;Fire;Hu Tao;UI_AvatarIcon_Side_Hutao;1940919994
        10000047;Wind;Kaedehara Kazuha;UI_AvatarIcon_Side_Kazuha;88505754
        10000048;Fire;Yanfei;UI_AvatarIcon_Side_Feiyan;697277554
        10000049;Fire;Yoimiya;UI_AvatarIcon_Side_Yoimiya;2504399314
        10000050;Fire;Thoma;UI_AvatarIcon_Side_Tohma;3555115602
        10000051;Ice;Eula;UI_AvatarIcon_Side_Eula;3717667418
        10000052;Electric;Raiden Shogun;UI_AvatarIcon_Side_Shougun;3024507506
        10000053;Wind;Sayu;UI_AvatarIcon_Side_Sayu;2388785242
        10000054;Water;Sangonomiya Kokomi;UI_AvatarIcon_Side_Kokomi;3914045794
        10000055;Rock;Gorou;UI_AvatarIcon_Side_Gorou;3400133546
        10000056;Electric;Kujou Sara;UI_AvatarIcon_Side_Sara;1483922610
        10000057;Rock;Arataki Itto;UI_AvatarIcon_Side_Itto;3068316954
        10000058;Electric;Yae Miko;UI_AvatarIcon_Side_Yae;2713453234
        10000059;Wind;Shikanoin Heizou;UI_AvatarIcon_Side_Heizo;646032090
        10000060;Water;Yelan;UI_AvatarIcon_Side_Yelan;2848374378
        10000062;Ice;Aloy;UI_AvatarIcon_Side_Aloy;3689108098
        10000063;Ice;Shenhe;UI_AvatarIcon_Side_Shenhe;334242634
        10000064;Rock;Yun Jin;UI_AvatarIcon_Side_Yunjin;655825874
        10000065;Electric;Kuki Shinobu;UI_AvatarIcon_Side_Shinobu;1940821986
        10000066;Water;Kamisato Ayato;UI_AvatarIcon_Side_Ayato;1588620330
        10000067;Grass;Collei;UI_AvatarIcon_Side_Collei;2948362178
        10000068;Electric;Dori;UI_AvatarIcon_Side_Dori;388272194
        10000069;Grass;Tighnari;UI_AvatarIcon_Side_Tighnari;2506955778
        10000070;Water;Nilou;UI_AvatarIcon_Side_Nilou;3850149970
        10000071;Electric;Cyno;UI_AvatarIcon_Side_Cyno;1049891906
        10000072;Water;Candace;UI_AvatarIcon_Side_Candace;3092975658
        10000073;Grass;Nahida;UI_AvatarIcon_Side_Nahida;712501082
        10000074;Ice;Layla;UI_AvatarIcon_Side_Layla;2889777514
        10000075;Wind;Wanderer;UI_AvatarIcon_Side_Wanderer;3230559562
        10000076;Wind;Faruzan;UI_AvatarIcon_Side_Faruzan;2387711994
        10000077;Grass;Yaoyao;UI_AvatarIcon_Side_Yaoyao;1732418482
        10000078;Grass;Alhaitham;UI_AvatarIcon_Side_Alhatham;4002157418
        10000005-501;None;Traveler;UI_AvatarIcon_Side_PlayerBoy;3816664530
        10000005-502;;#N/A;;
        10000005-503;;#N/A;;
        10000005-504;Wind;Traveler;UI_AvatarIcon_Side_PlayerBoy;1533656818
        10000005-505;;#N/A;;
        10000005-506;Rock;Traveler;UI_AvatarIcon_Side_PlayerBoy;1533656818
        10000005-507;Electric;Traveler;UI_AvatarIcon_Side_PlayerBoy;1533656818
        10000005-508;Grass;Traveler;UI_AvatarIcon_Side_PlayerBoy;1533656818
        10000007-701;None;Traveler;UI_AvatarIcon_Side_PlayerGirl;3816664530
        10000007-702;;#N/A;;
        10000007-703;;#N/A;;
        10000007-704;Wind;Traveler;UI_AvatarIcon_Side_PlayerGirl;3816664530
        10000007-705;;#N/A;;
        10000007-706;Rock;Traveler;UI_AvatarIcon_Side_PlayerGirl;3816664530
        10000007-707;Electric;Traveler;UI_AvatarIcon_Side_PlayerGirl;3816664530
        10000007-708;Grass;Traveler;UI_AvatarIcon_Side_PlayerGirl;3816664530";

        $lines = explode("\n", $text);

        $headers = explode(";", $lines[0]);
        $data = array_slice($lines, 1);

        $array = array_map(function($line) use ($headers) {
            $values = explode(";", $line);
            return array_combine($headers, $values);
        }, $data);

        print_r($array);
    }
}
