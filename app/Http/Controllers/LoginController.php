<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\AdminAccounts;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->json()->all();
        $password = md5($credentials['password']);

        $user = AdminAccounts::where('username', $credentials['username'])
                            ->where('password', $password)
                            ->first();

        if ($user) {
            // valid credentials
            return response()->json(['success' => true]);
        } else {
            // invalid credentials
            return response()->json(['success' => false]);
        }
    }
}
