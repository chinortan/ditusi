<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminAccountController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CsvController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/hello', function () {
    return "Hello World 3";
});

Route::get('/userScore/{userId}/{avatarId}', 'App\Http\Controllers\LeaderboardController@getUserScore');
Route::get('/user/{userId}', 'App\Http\Controllers\LeaderboardController@getUserInfo');


Route::get('/account', function () {
    return response()->json(['admin_accounts' => App\Models\User::all()]);
});

Route::get('admin-accounts', [AdminAccountController::class, 'index']);

Route::post('/login', 'App\Http\Controllers\LoginController@authenticate');

Route::post('/upload-csv/{id}', 'App\Http\Controllers\CsvController@uploadCsv');

//Route::post('/api/upload-csv/{id}', 'CsvController@upload');